package com.brandondev.backend.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "TotalsReport")
@Data
public class TotalsReport {
    @Id
    private String id;

    private int totalPageLoads;

    private double averageVisitInSeconds;
    private double averageUserVisitInSeconds;

    private int totalSessions;
    private int totalUsers;

    private Map<String, Integer> actionsTaken;
    private Map<String, Integer> actionsTakenOnSession;
    private Map<String, Integer> actionsTakenOnUser;
}
