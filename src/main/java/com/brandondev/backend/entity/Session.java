package com.brandondev.backend.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@Document(collection="Sessions")
public class Session {
    @Id
    private String id; //the sessionId assigned by UI
    private String userId;

    private Map<String, Integer> actionsTaken;
    private int totalTimeOnSiteInSeconds;
}
