package com.brandondev.backend.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Set;

@Document("DailyReports")
@Data
public class DailyReport {
    @Id
    private String id;

    private String key; //date string, 11-11-2022
    private Date date; //used for sorting

    int totalPageLoads;
    private Set<String> guids;
}
