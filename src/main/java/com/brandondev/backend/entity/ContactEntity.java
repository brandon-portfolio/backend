package com.brandondev.backend.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection="Contacts")
public class ContactEntity {
    @Id
    private String id;

    private String subject;
    private String message;
    private String contactEmail;

    private String ipAddress;
    private String sessionId;

    private Date date;
}
