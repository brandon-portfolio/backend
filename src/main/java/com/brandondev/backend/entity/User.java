package com.brandondev.backend.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;
import java.util.Set;

@Document(collection="Users")
@Data
public class User {
    @Id
    private String id;

    //used for determining a user across IP addresses and assigned guids
    private Set<String> guids;
    private Set<String> ipAddresses;

    private Map<String, Integer> actionsTaken;
    private int totalTimeOnSiteInSeconds;
}
