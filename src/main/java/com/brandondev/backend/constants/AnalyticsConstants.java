package com.brandondev.backend.constants;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnalyticsConstants {

    public static final String DB_SINGLE_ID = "ONLY-ONE";


    //non-user action
    public static final String PAGE_LOAD = "PAGE_LOAD";
    public static final String KEEP_ALIVE = "KEEP_ALIVE";

    //user actions from UI
    public static final String UI_RESUME = "RESUME";
    public static final String UI_VIEW_BPPROCESSING = "VIEW_BPPROCESSING";
    public static final String UI_VIEW_BLURAYS = "VIEW_BLURAYS";
    public static final String UI_VIEW_BLURAYS_SOURCE = "VIEW_BLURAYS_SOURCE";
    public static final String UI_VIEW_OLD = "VIEW_OLD";
    public static final String UI_VIEW_OLD_SOURCE = "VIEW_OLD_SOURCE";
    public static final String UI_VIEW_GITHUB_SOURCE = "VIEW_GITHUB_SOURCE";
    public static final String UI_SUBMIT_CONTACT = "SUBMIT_CONTACT";
    public static final String UI_SUBMIT_CONTACT_SUCCESS = "SUBMIT_CONTACT_SUCCESS";
    public static final String UI_VIEW_ANALYTICS = "VIEW_ANALYTICS";

    public static final String UI_LINK_LINKEDIN = "LINK_LINKEDIN";
    public static final String UI_LINK_GITLAB = "LINK_GITLAB";
    public static final String UI_LINK_GITHUB = "LINK_GITHUB";
    public static final String UI_LINK_EMAIL = "LINK_EMAIL";
    public static final String UI_LINK_FIGMA = "LINK_FIGMA";

    public static final List<String> ORDERED_UI_ACTIONS = Arrays.asList(
            UI_RESUME, UI_VIEW_ANALYTICS,
            UI_VIEW_BPPROCESSING, UI_VIEW_BLURAYS, UI_VIEW_BLURAYS_SOURCE, UI_VIEW_OLD, UI_VIEW_OLD_SOURCE, UI_VIEW_GITHUB_SOURCE,
            UI_SUBMIT_CONTACT, UI_SUBMIT_CONTACT_SUCCESS,
            UI_LINK_LINKEDIN, UI_LINK_GITLAB, UI_LINK_GITHUB, UI_LINK_EMAIL, UI_LINK_FIGMA);

    public static final Map<String, String> UI_ACTION_TO_EXTERNAL_NAME = new HashMap<String, String>() {{
        put(PAGE_LOAD,"Load Page");
        put(UI_RESUME,"Resume Button");
        put(UI_VIEW_BPPROCESSING,"View BP Processing");
        put(UI_VIEW_BLURAYS,"View Brandon's Blu Rays");
        put(UI_VIEW_BLURAYS_SOURCE,"View Blu Rays Source");
        put(UI_VIEW_OLD,"View Old Site");
        put(UI_VIEW_OLD_SOURCE,"View Old Site Source");
        put(UI_VIEW_GITHUB_SOURCE,"View Github Projects");
        put(UI_SUBMIT_CONTACT,"Submit Contact Forum");
        put(UI_VIEW_ANALYTICS,"View Analytics Button");
        put(UI_LINK_LINKEDIN,"Linkedin Link");
        put(UI_LINK_GITLAB,"Gitlab Link");
        put(UI_LINK_GITHUB,"Github Link");
        put(UI_LINK_EMAIL,"Email Link");
        put(UI_LINK_FIGMA,"Figma Link");
        put(UI_SUBMIT_CONTACT_SUCCESS, "Submit Contact Success");
    }};



}
