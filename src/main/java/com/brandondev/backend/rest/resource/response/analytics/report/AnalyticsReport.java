package com.brandondev.backend.rest.resource.response.analytics.report;

import lombok.Data;

import java.util.List;

@Data
public class AnalyticsReport {
    private int currentActiveUsers;
    private int currentActiveSessions;

    private int totalPageLoads;
    private int totalUniqueUsers;
    private String averageVisit;

    private List<YourActionsItem> yourActions;
    private List<TotalActionsItem> totalActions;

    private List<String> dailyChartLabels;
    private List<Integer> dailyViews;
    private List<Integer> dailyUsers;
}
