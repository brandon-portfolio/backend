package com.brandondev.backend.rest.resource.response;

import lombok.Data;

@Data
public class BasicErrorResponse {
    private String errorMessage;
    public BasicErrorResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
