package com.brandondev.backend.rest.resource.response.analytics.report;

import lombok.Data;

@Data
public class YourActionsItem {
    private String action;
    private String time;
}
