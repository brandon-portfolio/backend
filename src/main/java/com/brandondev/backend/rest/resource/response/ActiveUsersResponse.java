package com.brandondev.backend.rest.resource.response;

import lombok.Data;

@Data
public class ActiveUsersResponse {
    private int activeUsers;
    private int activeSessions;
}
