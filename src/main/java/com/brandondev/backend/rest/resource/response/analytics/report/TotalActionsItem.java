package com.brandondev.backend.rest.resource.response.analytics.report;

import lombok.Data;

@Data
public class TotalActionsItem {
    private String action;
    private int totalActions;
    private int userPercentage;
    private int sessionPercentage;
}
