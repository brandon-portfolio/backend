package com.brandondev.backend.rest.mapper;

import com.brandondev.backend.constants.AnalyticsConstants;
import com.brandondev.backend.entity.AnalyticsEntity;
import com.brandondev.backend.entity.DailyReport;
import com.brandondev.backend.entity.TotalsReport;
import com.brandondev.backend.rest.resource.response.analytics.report.AnalyticsReport;
import com.brandondev.backend.rest.resource.response.analytics.report.TotalActionsItem;
import com.brandondev.backend.rest.resource.response.analytics.report.YourActionsItem;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class AnalyticsMapper {

    private final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd");

    public AnalyticsReport mapToReport(TotalsReport totalsReport, List<DailyReport> dailyReports ) {
        AnalyticsReport report = new AnalyticsReport();

        report.setTotalPageLoads(totalsReport.getTotalPageLoads());
        report.setTotalUniqueUsers(totalsReport.getTotalUsers());
        report.setAverageVisit(""+(int)totalsReport.getAverageVisitInSeconds()+"Secs");

        report.setDailyChartLabels(new ArrayList<>());
        report.setDailyUsers(new ArrayList<>());
        report.setDailyViews(new ArrayList<>());

        for(DailyReport dailyReport : dailyReports) {
            report.getDailyChartLabels().add(dailyReport.getKey().substring(0,5));
            report.getDailyUsers().add(dailyReport.getGuids().size());
            report.getDailyViews().add(dailyReport.getTotalPageLoads());
        }

        List<TotalActionsItem> totalActionsItems = new ArrayList<>();

        for(String action : AnalyticsConstants.ORDERED_UI_ACTIONS) {
            if(totalsReport.getActionsTaken().containsKey(action)) {
                totalActionsItems.add(mapTotalActionsItem(totalsReport, action));
            }
        }

        report.setTotalActions(totalActionsItems);
        return report;
    }

    private TotalActionsItem mapTotalActionsItem(TotalsReport totalsReport, String action) {
        double sessionPercent = 0;
        if(totalsReport.getActionsTakenOnSession().containsKey(action) && totalsReport.getTotalSessions() != 0)
            sessionPercent = (double)totalsReport.getActionsTakenOnSession().get(action)/(double)totalsReport.getTotalSessions();
        sessionPercent*=100;

        double userPercent = 0;
        if(totalsReport.getActionsTakenOnUser().containsKey(action) && totalsReport.getTotalUsers() != 0)
            userPercent = (double)totalsReport.getActionsTakenOnUser().get(action)/(double)totalsReport.getTotalUsers();
        userPercent *= 100;

        TotalActionsItem item = new TotalActionsItem();
        item.setAction(AnalyticsConstants.UI_ACTION_TO_EXTERNAL_NAME.get(action));
        item.setTotalActions(totalsReport.getActionsTaken().get(action));
        item.setUserPercentage((int)userPercent);
        item.setSessionPercentage((int)sessionPercent);
        return item;
    }

    public List<YourActionsItem> mapToYourActions(List<AnalyticsEntity> analyticsEntities) {
        List<YourActionsItem> actions = new ArrayList<>();
        analyticsEntities.forEach(analyticsEntity -> {actions.add(mapToYourAction(analyticsEntity));});
        return actions;
    }

    public YourActionsItem mapToYourAction(AnalyticsEntity analyticsEntity) {
        YourActionsItem item = new YourActionsItem();
        item.setAction(AnalyticsConstants.UI_ACTION_TO_EXTERNAL_NAME.get(analyticsEntity.getAction()));
        item.setTime(simpleDateFormat.format(analyticsEntity.getDate()));
        return item;
    }
}
