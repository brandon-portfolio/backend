package com.brandondev.backend.rest.controller;

import com.brandondev.backend.entity.AnalyticsEntity;
import com.brandondev.backend.rest.service.AnalyticsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/analytics")
public class AnalyticsController extends BaseController {

    private AnalyticsService analyticsService;

    public AnalyticsController(HttpServletRequest httpServletRequest, AnalyticsService analyticsService) {
        super(httpServletRequest);
        this.analyticsService = analyticsService;
    }

    @GetMapping("/report")
    public ResponseEntity getReport() {
        return ResponseEntity.ok(analyticsService.getReport(getUserId()));
    }

    @PostMapping
    public ResponseEntity createEvent(@RequestBody AnalyticsEntity analyticsEntity) {
        analyticsEntity.setIpAddress(getIpHeader());
        analyticsEntity.setSessionId(getSessionId());
        analyticsEntity.setUserAgent(getUserAgent());
        analyticsEntity.setUserId(getUserId());


        analyticsService.createEvent(analyticsEntity);
        return ResponseEntity.ok(null);
    }
}
