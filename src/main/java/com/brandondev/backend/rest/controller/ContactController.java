package com.brandondev.backend.rest.controller;

import com.brandondev.backend.entity.ContactEntity;
import com.brandondev.backend.rest.error.ContactError;
import com.brandondev.backend.rest.resource.response.BasicErrorResponse;
import com.brandondev.backend.rest.service.ContactService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/contact")
public class ContactController extends BaseController {

    private ContactService contactService;

    public ContactController(HttpServletRequest servletRequest, ContactService contactService) {
        super(servletRequest);
        this.contactService = contactService;
    }

    @PostMapping
    public ResponseEntity createContact(@RequestBody ContactEntity contact) {
        contact.setIpAddress(getIpHeader());
        contact.setSessionId(getSessionId());

        try {
            contactService.createContact(contact);
            return ResponseEntity.ok(null);
        } catch (ContactError e) {
            return ResponseEntity.badRequest().body(new BasicErrorResponse(e.getContactErrorMessage()));
        }
    }

}
