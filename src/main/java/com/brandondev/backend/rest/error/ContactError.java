package com.brandondev.backend.rest.error;

public class ContactError extends Exception {
    private String contactErrorMessage;
    public ContactError(String contactErrorMessage) {
        this.contactErrorMessage = contactErrorMessage;
    }
    public String getContactErrorMessage() {
        return this.contactErrorMessage;
    }
}
