package com.brandondev.backend.rest.repository;

import com.brandondev.backend.entity.DailyReport;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DailyReportRepository extends MongoRepository<DailyReport, String> {

    public List<DailyReport> findByKeyIn(List<String> key);
}
