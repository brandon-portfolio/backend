package com.brandondev.backend.rest.repository;

import com.brandondev.backend.entity.AnalyticsEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AnalyticsRepository extends MongoRepository<AnalyticsEntity, String> {
    public Page<AnalyticsEntity> findByUserIdAndActionInOrderByDateDesc(String userId, List<String> action, PageRequest pageRequest);
}
