package com.brandondev.backend.rest.repository;

import com.brandondev.backend.entity.TotalsReport;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TotalsReportRepository extends MongoRepository<TotalsReport, String> {
}
