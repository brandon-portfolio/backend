package com.brandondev.backend.rest.repository;

import com.brandondev.backend.entity.ContactEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ContactRepository extends MongoRepository<ContactEntity, String> {
}
