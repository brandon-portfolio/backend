package com.brandondev.backend.rest.service;

import com.brandondev.backend.entity.AnalyticsEntity;
import com.brandondev.backend.rest.handler.AnalyticsHandler;
import com.brandondev.backend.rest.mapper.AnalyticsMapper;
import com.brandondev.backend.rest.resource.response.ActiveUsersResponse;
import com.brandondev.backend.rest.resource.response.analytics.report.AnalyticsReport;
import com.brandondev.backend.rest.utils.ActiveUserUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
public class AnalyticsService {

    private AnalyticsHandler analyticsHandler;
    private AnalyticsMapper analyticsMapper;

    AnalyticsService(AnalyticsHandler analyticsHandler, AnalyticsMapper analyticsMapper) {
        this.analyticsHandler = analyticsHandler;
        this.analyticsMapper = analyticsMapper;
    }

    public void createEvent(AnalyticsEntity analytics) {
        ActiveUserUtils.addEventOrRemove(analytics, null, null);
        analyticsHandler.createEvent(analytics);
    }

    public AnalyticsReport getReport(String userId) {
        AnalyticsReport report = analyticsHandler.getReport();
        int[] activeUsers = analyticsHandler.getActiveUsers();
        report.setCurrentActiveUsers(activeUsers[0]);
        report.setCurrentActiveSessions(activeUsers[1]);
        report.setYourActions(analyticsHandler.getAnalyticsForUserId(userId));
        return report;
    }
}
