package com.brandondev.backend.rest.service;

import com.brandondev.backend.entity.ContactEntity;
import com.brandondev.backend.rest.error.ContactError;
import com.brandondev.backend.rest.handler.ContactHandler;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class ContactService {
    private ContactHandler contactHandler;

    public ContactService(ContactHandler contactHandler) {
        this.contactHandler = contactHandler;
    }

    public void createContact(ContactEntity contact) throws ContactError {
        validateContactRequest(contact);
        contactHandler.createContact(contact);
    }

    private void validateContactRequest(ContactEntity contact) throws ContactError {
        if(contact.getContactEmail() == null || contact.getContactEmail().isEmpty())
            throw new ContactError("Please enter a email");
        if(contact.getMessage() == null || contact.getMessage().isEmpty())
            throw new ContactError("Please enter a message");
        if(contact.getSubject() == null || contact.getSubject().isEmpty())
            throw new ContactError("Please enter a subject");

        boolean isEmailValid = Pattern.compile("^(.+)@(\\S+)$").matcher(contact.getContactEmail()).matches();
        if(!isEmailValid)
            throw new ContactError("Please enter a valid email");
    }
}
