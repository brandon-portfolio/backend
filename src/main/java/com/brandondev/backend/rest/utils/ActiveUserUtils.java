package com.brandondev.backend.rest.utils;

import com.brandondev.backend.entity.AnalyticsEntity;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ActiveUserUtils {
    private static Map<String, Date> usersMap = new HashMap<>();
    private static Map<String, Date> sessionsMap = new HashMap<>();

    public static synchronized void addEventOrRemove(AnalyticsEntity analyticsEntity, Set<String> removeUsers, Set<String> removeSessions) {
        if(analyticsEntity != null ){
            if(analyticsEntity.getUserId() != null)
                usersMap.put(analyticsEntity.getUserId(), new Date());
            if(analyticsEntity.getSessionId() != null)
                sessionsMap.put(analyticsEntity.getSessionId(), new Date());
        }

        if(removeUsers != null) {
            for(String s : removeUsers)
                usersMap.remove(s);
        }

        if(removeSessions != null) {
            for(String s : removeSessions)
                sessionsMap.remove(s);
        }
    }


    public static int[] getTotalActiveUsersAndSessions() {
        Set<String> removableUsers = getRemovableItems(usersMap);
        Set<String> removableSessions = getRemovableItems(sessionsMap);

        addEventOrRemove(null, removableUsers, removableSessions);

        return new int[] {usersMap.size(), sessionsMap.size()};
    }

    private static Set<String> getRemovableItems(Map<String, Date> items) {
        Set<String> removeItems = new HashSet<>();
        for(String key : items.keySet()) {
            if(items.get(key).before(new Date(System.currentTimeMillis()- TimeUnit.MINUTES.toMillis(5))))
                removeItems.add(key);
        }
        return removeItems;
    }



}
