package com.brandondev.backend.rest.handler;

import com.brandondev.backend.constants.AnalyticsConstants;
import com.brandondev.backend.entity.AnalyticsEntity;
import com.brandondev.backend.entity.DailyReport;
import com.brandondev.backend.entity.TotalsReport;
import com.brandondev.backend.rest.mapper.AnalyticsMapper;
import com.brandondev.backend.rest.repository.AnalyticsRepository;
import com.brandondev.backend.rest.repository.DailyReportRepository;
import com.brandondev.backend.rest.repository.TotalsReportRepository;
import com.brandondev.backend.rest.resource.response.analytics.report.AnalyticsReport;
import com.brandondev.backend.rest.resource.response.analytics.report.YourActionsItem;
import com.brandondev.backend.rest.utils.ActiveUserUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class AnalyticsHandler {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");

    private AnalyticsRepository analyticsRepository;
    private DailyReportRepository dailyReportRepository;
    private TotalsReportRepository totalsReportRepository;
    private AnalyticsMapper analyticsMapper;

    public AnalyticsHandler(AnalyticsRepository analyticsRepository, DailyReportRepository dailyReportRepository,
                            TotalsReportRepository totalsReportRepository, AnalyticsMapper analyticsMapper) {
        this.analyticsRepository = analyticsRepository;
        this.dailyReportRepository = dailyReportRepository;
        this.totalsReportRepository = totalsReportRepository;
        this.analyticsMapper = analyticsMapper;
    }

    public void createEvent(AnalyticsEntity analytics) {
        analytics.setId(UUID.randomUUID().toString());
        analytics.setDate(new Date());
        analytics.setLogged(false);
        analyticsRepository.save(analytics);
    }

    @Cacheable("analyticsReport")
    public AnalyticsReport getReport() {
        TotalsReport totalsReport = getTotalsReport();
        List<DailyReport> dailyReports = getDailyReports();
        return analyticsMapper.mapToReport(totalsReport, dailyReports);
    }

    public List<YourActionsItem> getAnalyticsForUserId(String userId) {
        if(userId == null)
            return new ArrayList<>();

        List<String> actions = new ArrayList<>(AnalyticsConstants.ORDERED_UI_ACTIONS);
        actions.add(AnalyticsConstants.PAGE_LOAD);

        Page<AnalyticsEntity> analyticsEntities = analyticsRepository.findByUserIdAndActionInOrderByDateDesc(userId, actions, PageRequest.of(0,15));
        return analyticsMapper.mapToYourActions(analyticsEntities.getContent());
    }

    public TotalsReport getTotalsReport() {
        Optional<TotalsReport> dbReport = totalsReportRepository.findById(AnalyticsConstants.DB_SINGLE_ID);
        if(dbReport.isPresent())
            return dbReport.get();
        TotalsReport report = new TotalsReport();
        report.setTotalPageLoads(0);
        report.setAverageVisitInSeconds(0);
        report.setAverageUserVisitInSeconds(0);

        report.setTotalSessions(0);
        report.setTotalUsers(0);

        report.setActionsTakenOnSession(new HashMap<>());
        report.setActionsTakenOnUser(new HashMap<>());
        report.setActionsTaken(new HashMap<>());
        return report;
    }


    //returns a list of sorted daily reports
    //will fill in missing reports with empty daily reports
    private List<DailyReport> getDailyReports() {
        List<String> keys = new ArrayList<>();

        for(int i = 30; i >= 0; i--) {
            LocalDate date = LocalDate.now().minusDays(i);
            keys.add(date.format(formatter));
        }

        List<DailyReport> databaseReports = dailyReportRepository.findByKeyIn(keys);
        Map<String, DailyReport> dailyReportMap = new HashMap<>();
        databaseReports.forEach(dailyReport -> {dailyReportMap.put(dailyReport.getKey(), dailyReport);});

        List<DailyReport> reports = new ArrayList<>();
        for(String key : keys) {
            if(dailyReportMap.containsKey(key)) {
                reports.add(dailyReportMap.get(key));
            } else {
                DailyReport report = new DailyReport();
                report.setKey(key);
                report.setGuids(new HashSet<>());
                report.setTotalPageLoads(0);
                reports.add(report);
            }
        }
        return reports;
    }

    public int[] getActiveUsers() {
        return ActiveUserUtils.getTotalActiveUsersAndSessions();
    }
}
