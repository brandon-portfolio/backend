package com.brandondev.backend.rest.handler;

import com.brandondev.backend.entity.ContactEntity;
import com.brandondev.backend.rest.repository.ContactRepository;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class ContactHandler {
    private ContactRepository contactRepository;

    public ContactHandler(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    public void createContact(ContactEntity contact) {
        contact.setId(UUID.randomUUID().toString());
        contact.setDate(new Date());
        contactRepository.save(contact);
    }
}
